# -*- coding: utf-8 -*-

"""
Created for test
@date:      03/08/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""

class NodeObject(object):
    """
    Comment for objectclass
    """

    def __init__(self, gene_id, gene_name="NA", node_list=None, predicted=False):
        super(NodeObject, self).__init__()

        self.wormbase_id = gene_id
        self.label = gene_name
        self.parent_nodes = []
        self.interactions = []
        self.predicted = predicted

        if not node_list:
            self.node_object_list = []
        else:
            self.node_object_list = node_list


    def add_parent(self, node):
        if node in self.parent_nodes: return
        self.parent_nodes.append(node)

    def add_node(self, node):
        if node in self.node_object_list: return
        self.node_object_list.append(node)

    def add_interaction(self, interaction):
        if interaction in self.interactions: return
        self.interactions.append(interaction)
