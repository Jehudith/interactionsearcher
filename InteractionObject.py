# -*- coding: utf-8 -*-


"""
Created for InteractionObject
@date:      03/08/2016
@author:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""

from enum import Enum

class InteractionObject(object):
    """
    Comment for objectclass
    """

    def __init__(self, id, effector, affected, type):
        super(InteractionObject, self).__init__()
        self.id = id
        self.effector = effector
        self.affected = affected
        self.type = type

    @staticmethod
    def get_interaction_type(type):
        # Loop all members to see if an enum type matches the string in WB,
        # returns InteractionType.Unknown when does not exist
        for name, member in InteractionType.__members__.items():
            if member.value == type:
                return member
        print("Interaction type not found: " + type)
        return InteractionType.Unknown


class InteractionType(Enum):
    Predicted = "Predicted"
    ChangeOfLocalization = "Change of localization"
    ChangeOfExpression = "Change of expression level"
    DoesNotRegulate = "Does Not Regulate"
    Regulatory = "Regulatory"
    GeneticInteraction = "Genetic interaction"
    Suppression = "Suppression"
    ProteinProtein = "ProteinProtein"
    Synthetic = "Synthetic"
    ProteinDNA = "ProteinDNA"
    Enhancement = "Enhancement"
    NoInteraction = "No interaction"
    Epistasis = "Epistasis"
    Asynthetic = "Asynthetic"
    UnilateralSuppression = "Unilateral suppression"
    PhenotypeBias = "Phenotype bias"
    NegativelyRegulates = "Negatively Regulates"
    MutualEnhancement = "Mutual enhancement"
    PartialSuppression = "Partial suppression"
    ProteinRNA = "ProteinRNA"
    PositivelyRegulates = "Positively Regulates"
    MutualSuppression = "Mutual suppression"
    UnilateralEnhancement = "Unilateral enhancement"
    MaximalEpistasis = "Maximal epistasis"
    Physical = "Physical"
    Oversuppression = "Oversuppression"
    CompleteMutualSuppression = "Complete mutual suppression"
    OversuppressionEnhancement = "Oversuppression enhancement"
    OpposingEpistasis = "Opposing epistasis"
    CompleteSuppression = "Complete suppression"
    MinimalEpistasis = "Minimal epistasis"
    PartialUnilateralSuppression = "Partial unilateral suppression"
    Unknown = "Unknown Type"


