# -*- coding: utf-8 -*-

"""
Created for InteractionSearcher
@date:      02/08/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
import collections
import os

from InteractionObject import InteractionObject
from NodeObject import NodeObject
from config import PYTHON_PATH_DIR
from handlers.requesthandlers.WormbaseRequestHandler import WormbaseRequestHandler
import subprocess

class Controller(object):

    def __init__(self):
        self.wb = WormbaseRequestHandler()
        self.lookup_dict = {}

    def get_interactions(self, edges, main_node):
        interaction_list = []
        affected_node = None
        effector_node = None

        if not edges:
            return

        # Loop edges
        for edge in edges:
            # Get data
            edge_type = edge["type"]
            affected_id = edge["affected"]["id"]
            if affected_id in self.lookup_dict:
                affected_node = self.lookup_dict[affected_id]
            else:
                affected_node = self.create_node(affected_id, edge["affected"])

            effector_id = edge["effector"]["id"]
            if effector_id in self.lookup_dict:
                effector_node = self.lookup_dict[effector_id]
            else:
                effector_node = self.create_node(effector_id, edge["effector"])

            # Create interaction objects
            for interaction in edge["interactions"]:
                interaction_object = InteractionObject(interaction["id"], effector_node, affected_node,
                                                       InteractionObject.get_interaction_type(edge_type))
                interaction_list.append(interaction_object)

            # Couple nodes
            child_node = None
            if affected_node.wormbase_id == main_node.wormbase_id:
                child_node = effector_node
            else:
                child_node = affected_node

            main_node.add_node(child_node)
            child_node.add_parent(main_node)

        # Add interaction objects to the main node
        for interaction_node in interaction_list:
            main_node.add_interaction(interaction_node)

    def process_data(self, gene_id):
        """
        From wormbase the Interaction JSON is retrieved and decoded. Only the part of the Nodes is of interest. In the Nodes dict
        all the information of the interaction genes is given
        :param gene_id: For each gene id the data is processed here

        """
        # Get data from Wormbase, its a dict
        interaction_dict = self.wb.get_interaction(gene_id)["interactions"]
        interactions_data = interaction_dict["interactions"]["data"]
        main_node = self.create_node(gene_id, interaction_dict["name"]["data"])

        # some gene have edges_all and some edges
        edges = None
        if "edges" in interactions_data:
            edges = interactions_data["edges"]
        elif "edges_all" in interactions_data:
            edges = interactions_data["edges_all"]
        else:
            print("Could not find edges type for gene: {0}".format(gene_id))

        # Get interactions
        self.get_interactions(edges, main_node)

        # Only process nodes when they are available
        if not "nodes" in interactions_data:
            print("{0} has no nodes, processed edges".format(gene_id))
            return

        # Put the information of nodes in the retrieved dict
        retrieved_dict = interactions_data["nodes"]

        # Some gene do not have any interaction
        if retrieved_dict is None:
            return

        # The other genes are the interactors and will be added to this list.
        list_with_nodes = []


        # Get all ids from dict
        for id in retrieved_dict:
            # Find the inserted gene id and add his information to the main node dict
            if id == gene_id:
                continue
            # test
            #print("main node must be filled", main_node)

            # Add all the other interaction gene IDs with their dict in list
            else:
                list_with_nodes.append(retrieved_dict[id])

        # This function makes for each gene an Node Object
        # Gene Nodes are the dicts with information about the gene, including geneID
        self.couple_node_objects(main_node, list_with_nodes)

    def process_data_into_variables(self, to_process_dict):
        """
        This function puts the information of the dict in variables
        :param to_process_dict: Is the dict with information of one gene
        :return: the created variables are returned: gene id and gene name
        """
        gene_id = ""
        gene_name = ""
        predicted = False

        if "id" in to_process_dict:
            gene_id = to_process_dict["id"]
        if "label" in to_process_dict:
            gene_name = to_process_dict["label"]
        if "predicted" in to_process_dict:
            if to_process_dict["predicted"] == 1:
                predicted = True
        return gene_id, gene_name, predicted

    def couple_node_objects(self, main_node, list_with_nodes):
        """
        Make Node Objects with the information from the dicts
        :param main_node: Has the information of the inserted gene ID
        :param list_with_nodes: Has all the interactor Genes with their information
        :return: main_node_object
        """

        # Loop over all the keys in the interaction list
        for node_item in list_with_nodes:
            # When the Key is ID put in the variable
            node_id = node_item["id"]
            # Create Node object with the gene id and its information (node_item)
            node_object = self.create_node(node_id, node_item)
            # add every object to list
            main_node.add_node(node_object)

            # Add parent to node, the main gene object is also added
            node_object.add_parent(main_node)

        return main_node

    def create_node(self, gene_id, node_item):
        """
        This function makes an Object for every inserted gene. (main node)
        :param gene_id: Id from the inserted gene
        :param node_item: The information in dict of the inserted geneid
        :return: Node object
        """
        # Check if already in lookup dict
        if gene_id in self.lookup_dict:
            # Is in lookup, get node object
            node_object = self.lookup_dict[gene_id]
        else:
            # Not in lookup, create Node and add to lookup
            # Put the gene name and id in variables
            gene_id, label, predicted = self.process_data_into_variables(node_item)
            # Create the node object
            node_object = NodeObject(gene_id, label, predicted=predicted)
            # This object linked to the specific id
            self.lookup_dict[gene_id] = node_object
        return node_object

    def print_node_count(self, count_dict, title):
        print("\n")
        print(title)
        print("\n")
        for key, value in count_dict.items():
            if value < 2: continue
            print("{0} : {1}".format(key, value))

    def calculate_totals(self, count_list, field_list):
        dict_total = {}
        for name in field_list:
            for count_dict in count_list:
                if not name in count_dict.keys(): continue
                if not name in dict_total:
                    dict_total[name] = 0
                dict_total[name] += count_dict[name]
        return dict_total

    def increase_node_count(self, count_dict, label):

        # Check if node already exists
        if not label in count_dict:
            count_dict[label] = 0

        # Increase count
        count_dict[label] += 1

    def undup(self, seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    def print_node_relations(self, original_gene_list):
        # List of genes without interaction
        no_interaction_list = []

        # This list saves the inserted genes which have a interaction
        between_interactions_dict = {}

        # Dict with counts
        count_dict = {}
        count_dict_common = {}

        print("\n")
        print("Interactions between gene list:")
        print("\n")

        # loop all parent_nodes and check if they have interaction with other parent_nodes
        for original_gene_id in original_gene_list:
            if not original_gene_id in self.lookup_dict:
                no_interaction_list.append(original_gene_id)
                continue
            parent_node = self.lookup_dict[original_gene_id]
            node_list = parent_node.node_object_list

            # Loop interaction list to see if matches found
            for gene_node in node_list:
                # Filter out predicted
                if gene_node.predicted:
                    continue

                # Interactor in inserted list. gene_node is an object and has an id attribute
                if gene_node.wormbase_id in original_gene_list:
                    # Label is the gene name
                    if not parent_node.label in between_interactions_dict:
                        # Add gene name in the between interaction dict
                        between_interactions_dict[parent_node.label] = gene_node.label
                    else:
                        # if the gene label already exists add ,
                        between_interactions_dict[parent_node.label] += ", " + gene_node.label
                    self.increase_node_count(count_dict, gene_node.label)

        # Print alphabetic between interaction and separated by ,
        odb = collections.OrderedDict(sorted(between_interactions_dict.items(), key=lambda s: s[0].lower()))
        for key, value in odb.items():
            self.increase_node_count(count_dict, key)
            print(key + " -> " + value)

        # print count
        #self.print_node_count(count_dict, "Interactions between gene list (Count):")

        # Common interactions
        common_interactions_dict = {}
        print("\n")
        print("Common interactions:")
        print("\n")

        for gene_id, gene_node in self.lookup_dict.items():
            if len(gene_node.parent_nodes) < 2:
                continue

            # Filter out predicted
            if gene_node.predicted:
                continue

            # Has parent nodes
            parent_string = ""
            for index, parent_node in enumerate(gene_node.parent_nodes):
                parent_string += parent_node.label
                self.increase_node_count(count_dict_common, parent_node.label)
                if index < len(gene_node.parent_nodes)-1:
                    parent_string += ", "
            if not parent_string in common_interactions_dict:
                common_interactions_dict[parent_string] = gene_node.label
            else:
                common_interactions_dict[parent_string] += ", " + gene_node.label
            self.increase_node_count(count_dict_common, gene_node.label)

        # print common interactions
        od = collections.OrderedDict(sorted(common_interactions_dict.items(), key=lambda s: s[0].lower()))
        for key, value in od.items():
            print(key + " -> " + value)

        # print count
        #self.print_node_count(count_dict_common, "Common interactions (Count):")

        # print total count
        fields_names = []
        fields_names.extend(count_dict.keys())
        fields_names.extend(count_dict_common.keys())
        fields_names = self.undup(fields_names)
        self.print_node_count(self.calculate_totals([count_dict, count_dict_common], fields_names), "Total Count:")

        # Print nodes with no interactions
        print("\n")
        print("No interactions found for following nodes: " + str(no_interaction_list))
        print("Number of genes not found", len(no_interaction_list))
        print("\n")

    def get_tree(self, original_gene_list, filter=None):
        """
        :param original_gene_list: The gene list which was the input
        :return: This function prints the results in the command line
        """
        if filter is None: filter = []

        # List with valid interaction objects
        interaction_object_list = []

        # loop all parent_nodes and check if they have interaction with other parent_nodes
        for original_gene_id in original_gene_list:
            if not original_gene_id in self.lookup_dict:
                continue

            # Get the interaction objects
            parent = self.lookup_dict[original_gene_id]
            interactions = parent.interactions

            # Filter interactions
            filtered_interactions = [x for x in interactions if x.type not in filter]

            # Add valid interactions to list
            interaction_object_list.extend(filtered_interactions)

        # Make tree
        self.create_tree_file(interaction_object_list)

    def create_tree_file(self, interactions):
        s = "blockdiag { "
        #s = "blockdiag { orientation = portrait "

        # Build tree
        for interaction in interactions:
            s += "   {0} -> {1} [label = \"{2}\", textcolor=\"red\", fontsize = 6];".format(interaction.effector.label, interaction.affected.label, interaction.type.value)

        # Close tree
        s += "}"

        # Save file
        filename = "result.diag"
        filenamePic = "result.pdf"
        file_handler = open(filename, "wt")
        file_handler.write(s)
        file_handler.close()

        # Run command
        thisdir = os.path.dirname(os.path.realpath(__file__))
        filedir = os.path.abspath(os.path.join(thisdir, os.pardir))
        subprocess.check_call([os.path.join(PYTHON_PATH_DIR, 'blockdiag.exe'), '-Tpdf'] +
           ' {0}'.format(filename).split())

        # Open file
        os.startfile(filedir+"/"+filenamePic, 'open')


