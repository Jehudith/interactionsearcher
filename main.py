# -*- coding: utf-8 -*-

"""
Created for InteractionSearcher
@date:      30/07/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""
import time

from InteractionObject import InteractionType, InteractionObject
from controller.Controller import Controller

def input_list_genes():

    gene_list = []
    while True:
        gene = input("")

        if gene == "":
            break
        gene_list.append(gene)
    return gene_list

if __name__ == "__main__":
    ## test dict
    #retrieved_dict = {'WBGene00014172': {'id': 'WBGene00014172', 'class': 'gene', 'taxonomy': 'c_elegans', 'predicted': 1, 'label': 'clpp-1'}, 'WBGene00019461': {'id': 'WBGene00019461', 'class': 'gene', 'taxonomy': 'c_elegans', 'predicted': 1, 'label': 'K07A3.3'}, 'WBGene00008412': {'id': 'WBGene00008412', 'class': 'gene', 'taxonomy': 'c_elegans', 'predicted': 1, 'label': 'D2030.2'}, 'WBGene00013541': {'taxonomy': 'c_elegans', 'predicted': 1, 'main': 1, 'label': 'Y75B8A.4', 'id': 'WBGene00013541', 'class': 'gene'}, 'WBGene00009664': {'id': 'WBGene00009664', 'class': 'gene', 'taxonomy': 'c_elegans', 'predicted': 1, 'label': 'idha-1'}}

    c = Controller()

    print("Insert genes:\n")

    ## Original gene list is the list of genes which were insert in the input
    original_gene_list = input_list_genes()

    ## Process data of every gene with delay to not spam Wormbase
    for gene_id in original_gene_list:
        c.process_data(gene_id)
        time.sleep(0.2)

    print("\nReady to build tree")

    filter = []

    # Always filter out predicted
    filter.append(InteractionType.Predicted),

    while True:
        filter_str = "Currently in interaction filter: "
        for item in filter:
            filter_str += "{0}, ".format(item.value)
        if len(filter_str) > 2: filter_str = filter_str[:-2]
        print(filter_str+"\n")
        cmd = input("Filter, exit, print or build: \n")

        if cmd == "exit":
            break

        if cmd == "build":
            # Build tree
            c.get_tree(original_gene_list, filter=filter)
            continue

        if cmd == "print":
            c.print_node_relations(original_gene_list)
            continue

        # Try to get enum
        enum = InteractionObject.get_interaction_type(cmd)
        if enum == InteractionType.Unknown:
            print("Not a valid filter!\n")
            continue

        if enum in filter:
            filter.remove(enum)
        else:
            filter.append(enum)

